# How NOT to make a GDD with org-roam


## Context
I'm relatively new to Emacs, but vanilla Emacs is not for me so I installed Doom Emacs, a well known distribution. Days after I learned about org-mode and simply love it, but I went further and tried org-roam... and blew my mind, it was so simple get so elegant, I was looking for a way of linking files so every file I make has a little bit of information and I won't get lost trying to find some detail about something in a 5000 lines file.

I started recently to develop a videogame, name pending, and of course I just skiped making a design document because why whould you need that... right?, well I was wrong. I have a few examples of GDD thanks to university courses and wanted to make my own GDD. I searched for online alternatives for this and find [Nuclino](https://www.nuclino.com/), made an account and tried to export the example that they have but you can only export certain categories, not the whole GDD (or at least I couldn't), besides I'm trying to solo develop and an online solution was overkill in my opinion. 


I opened Emacs and started making org file for every category, make a "hub" to have all the links called "GDD.org" (for easy acceses to every file) and exported it with ```org-latex-export-to-pdf``` and ran into problems...

## TL;DR
I want to use org-roam for making a large pdf with smaller org files.

## My "solution"

### Pros:
    - Easy if you know a little about emacs and have time.
    - You maybe learn something new.
### Cons:
    - It is tedious.

## Step 1
  Try to find another way, ask on internet.
  
## Step 2
  You find a better way?... tell [me](mailto:leonardo.sobarzo@protonmail.com) please. 
  
## Step 3

### Let's begin

If you export a org-roam file with links in it with ```org-latex-export-to-pdf``` it won't add the content of the other files to the exported pdf but you can use INCLUDE for this ([thanks to NickD](https://emacs.stackexchange.com/questions/68309/how-to-correctly-export-to-pdf-org-roam-files)):

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es

#+INCLUDE: <path_to_file>
#+INCLUDE: <path_to_file_other_file>
#+INCLUDE: <path_to_file_other_file>
...
```

Pretty simple, right?. Well we have a lots of problems now:

### Problems
  1. Table of contents too big if you have a lots of levels in the files.
  2. Table of contents is in the cover.
  3. Roam ID it being exported to the file.
  4. Every file has a title and all the titles are in the exported pdf... and they are in the cover.

### Solutions

We need to add a few modifications to eliminate the previous problems:

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es
#+OPTIONS: toc:nil

#+begin_export latex
  \clearpage \tableofcontents \clearpage
#+end_export latex

#+INCLUDE: <path_to_file> :lines "5-"
#+INCLUDE: <path_to_file_other_file> "5-"
#+INCLUDE: <path_to_file_other_file> "5-"
...
```

\* TOC will only be 2 levels deep per category. 

### More problems?
  1. If you want the file in english well you don't have this problem, but if you need it in spanish like me (or in any other language) the header ```#+LANGUAGE: es``` didn't do anything.
  2. You forgot about having links to the files...

### More solutions!

You could put as header:
```
...
#+LANGUAGE: es
#+latex_header: \usepackage[<language>]{babel}
...
```

Until here was just tedious putting the various ```#+INCLUDE``` and having to order by hand, but here comes the **_good part_**. If you have linked files in org-roam you know how they are linked, you just use ```org-roam-node-insert``` or if you have enabled ```completion-at-point``` in your config.el, emacs will try to find a node with what you are writting and autocomplete. If you try to delete the link you will find that the link is just [[id:numbers]\[name\_of\_file\]], in my case my files names are "GDD-name\_of\_proyect:secction", so it's kind of long and redundant because all the file is about the same proyect, but you can change to second [] to be whatever you want. So having in mind that you can change the name of the link but it will still link the correct file. **_My solution_** is:

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es
#+OPTIONS: toc:nil

#+begin_export latex
  \clearpage \tableofcontents \clearpage
#+end_export latex

* [[id:...][short_name]]
#+INCLUDE: <path_to_file> :lines "5-"

* [[id:...][other_short_name]]
#+INCLUDE: <path_to_file_other_file> "5-"

* [[id:...][other_short_name]]
#+INCLUDE: <path_to_file_other_file> "5-"
```

## This solution is not for everyone
This "solution" is tedious and still have problems like: what if I don't want to have the name in plain text and want a picture like in game or the links to the files are still in the pdf so you can click them and open the raw org-roam files, this will give error if you give the exported file to another person.

My GDD is still too young to have to think about this issues, if I have find a solution I will post it.

{{< admonition danger >}}
**This is not a professional solution in any case, and I think this is only usefull for a solo developer.**
{{< /admonition >}}

## Your obvious reaction
{{< image src="latex-roam.jpg" caption="latex with extra steps" height="1562" width="2400" >}}

Then why I use org-roam for this?

{{< image src="its_neat.jpg" caption="it's neat" height="1562" width="2400">}}

