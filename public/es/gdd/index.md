# Como NO hacer un GDD con org-roam


## Contexto
Soy relativamente nuevo en Emacs, probé Emacs básico y no es lo mio así que instalé Doom Emacs, una conocida distribución. Dias después aprendí la existencia de org-mode y lo amé, pero fuí más allá e intenté org-roam... y fue hermoso, era tan simple pero aun así elegante, Estaba buscando una forma de enlazar archivos para que cada archivo tuviese un poco de información y no terminara perdido tratando de buscar algún detalle sobre algo en un arhivo de 5000 lineas.

Recientemente comencé a desarrollar un videojuego, y por supuesto no hice un documento de diseño porque quién necesita eso... ¿cierto?, estaba equivocado. Tengo algunos ejemplos de GDD gracias a cursos de la universidad y quería hacer mi propio GDD. Busqué una alternativa en linea y encontre [Nuclino](https://www.nuclino.com/), me hice una cuenta e intenté exportar el ejemplo pero sólo pude exportar por categorias, no pude exportar el GDD completo (o al menos no supe como), además estoy intentando de desarrollar el juego solo y una solución en linea era excesiva en mi opinion.

Abrí Emacs y comencé a generar archivos org para cada categoria, generé un archivo base llamado "GDD.org" que mantuviese todos los enlaces (para acceso fácil) y lo exporté con ```org-latex-export-to-pdf``` y me topé con problemas...

## TL;DR
Quiero usar org-roam para hacer un pdf gigante con archivo más pequeños.

## Mi "solución"

### Pros:
    - Fácil si ya conoces algo de emacs y tienes tiempo.
    - Quizás aprendas algo nuevo.
### Cons:
    - Es tediosa.

## Step 1
  Intenta encontrar otra solución, busca en internet.
  
## Step 2
  ¿Encontraste algo mejor?... [dime](mailto:leonardo.sobarzo@protonmail.com) por favor.
  
## Step 3

### Comencemos

Si has exportado archivos org-roam con enlaces con ```org-latex-export-to-pdf``` no se añade el contenido de los otros archivos al pdf exportado, para esto puedes utilizar INCLUDE ([gracias a NickD](https://emacs.stackexchange.com/questions/68309/how-to-correctly-export-to-pdf-org-roam-files)):

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es

#+INCLUDE: <path_to_file>
#+INCLUDE: <path_to_file_other_file>
#+INCLUDE: <path_to_file_other_file>
...
```

Bastante simple, ¿verdad?. Ahora tenemos varios problemas:

### Problemas
  1. Tabla de contenidos es demaciado grande si tienes muchos niveles en los archivos.
  2. Tabla de contenidos está en la portada.
  3. Los identificadores de Roam se exportan.
  4. Cada archivo posee un título y todos están unidos en el pdf exportado... y están en la portada.

### Soluciones

Necesitamos añadir algunas modificaciones para eliminar los errores previos:

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es
#+OPTIONS: toc:nil

#+begin_export latex
  \clearpage \tableofcontents \clearpage
#+end_export latex

#+INCLUDE: <path_to_file> :lines "5-"
#+INCLUDE: <path_to_file_other_file> "5-"
#+INCLUDE: <path_to_file_other_file> "5-"
...
```

\* La tabla de contenidos solo tendrá dos nivelese de profundidad por categoría.

### ¿Más problemas?
  1. Si necesitas el archivo en ingles no existe problema, pero si lo necesitas en español (u otro idioma) el encabezado ```#+LANGUAGE: es``` no tradujo nada.
  2. ¿Dónde quedaron los enlaces a los otros archivos, los olvidaste? 

### ¡Más soluciones!

Podemos poner el encabezado:
```
...
#+LANGUAGE: es
#+latex_header: \usepackage[<language>]{babel}
...
```

Hasta aquí sólo ha sido tedioso tener que poner ```#+INCLUDE``` y tener que ordenarlos a mano, pero aquí viene **_lo bueno_**. Si has enlazado archivos en org-roam ya sabes que hay que hacer, utilizar ```org-roam-node-insert``` o tener habilitado ```completion-at-point``` en tu config.el, emacs intentará autocompletar lo que escribes con algún nodo. Si intentas borrar el link te darás cuenta que el enlace es sólo [[id:numeros]\[nombre\_archivo\]], en mi caso los archivos se llaman "GDD-nombre_proyecto:seccion", son largos y redundantes porque todo en este archivo es sobre el mismo proyecto, pero tu puedes cambiar el segundo [] a lo que desees. Teniendo eso en mente puedes cambiar los nombres de los enlaces pero seguirán apuntando al archivo en cuestión. **_Mi solución_** es:

```
#+Title: GDD or something like that
#+AUTHOR: you name or company
#+LANGUAGE: es
#+OPTIONS: toc:nil

#+begin_export latex
  \clearpage \tableofcontents \clearpage
#+end_export latex

* [[id:...][short_name]]
#+INCLUDE: <path_to_file> :lines "5-"

* [[id:...][other_short_name]]
#+INCLUDE: <path_to_file_other_file> "5-"

* [[id:...][other_short_name]]
#+INCLUDE: <path_to_file_other_file> "5-"
```

## Esta solución no es para todos
Esta "solución" es tediosa y aun tiene problemas como: qué pasa si no quiero tener el nombre del juego en texto plano quiero una imagen o los enlaces de los archivos se exportan al pdf así que puedes presionarlos y se abrirá el archivo org, esto le dará un error a otra persona que le pases el pdf.

Mi GDD está aun demaciado joven para pensar sobre estos problemas, si encuentro una solución la colocaré aquí.

{{< admonition danger >}}
**Esta no es una solución profesional en ningún caso, y creo que sólo es útil para alguien que esté desarrollando solo.**
{{< /admonition >}}

## Tu obvia reacción
{{< image src="latex-roam.jpg" caption="latex with extra steps" height="1562" width="2400" >}}

¿Entonces por qué utilizo org-roam para esto?
{{< image src="its_neat.jpg" caption="It's neat" height="1562" width="2400">}}

