---
title: "About"
date: 2021-08-31
draft: false

lightgallery: true

math:
  enable: true
---

Soy un estudiante universitario de informática, que cursa sus últimos años. Me hice este blog para aprender que es hugo y compartir lo poco y nada de conocimiento que tengo.

Estoy interesado en el desarrollo de juegos y la programación en general.

Rellenaré más el about cuando tenga algo que escribir.

{{< admonition question >}}
No se por qué pero el icono de la página desaparece al estar en español, ¿qué será?.
{{< /admonition >}}
